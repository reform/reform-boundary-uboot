// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright 2018 Boundary Devices
 */

/dts-v1/;

#include "fsl-imx8mq.dtsi"

&iomuxc {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_hog>;

	iomuxc_pinctrl: iomuxc-pinctrlgrp {
	};
};

&iomuxc_pinctrl {
	pinctrl_bt_rfkill: bt-rfkillgrp {
		fsl,pins = <
#define GP_BT_RFKILL_RESET	<&gpio3 19 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_SAI5_RXFS_GPIO3_IO19		0x19
		>;
	};

	pinctrl_ecspi2: ecspi2grp {
		fsl,pins = <
			/* unused on our carrier */
#define GP_ECSPI2_CS0	<&gpio5 13 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_ECSPI2_SS0_GPIO5_IO13		0x19	/* Pin 89 */
			MX8MQ_IOMUXC_ECSPI2_MOSI_ECSPI2_MOSI		0x19	/* Pin 91 */
			MX8MQ_IOMUXC_ECSPI2_MISO_ECSPI2_MISO		0x19	/* Pin 93 */
			MX8MQ_IOMUXC_ECSPI2_SCLK_ECSPI2_SCLK		0x19	/* Pin 97 */
		>;
	};

	pinctrl_fec1: fec1grp {
		fsl,pins = <
			MX8MQ_IOMUXC_ENET_MDC_ENET1_MDC			0x3
			MX8MQ_IOMUXC_ENET_MDIO_ENET1_MDIO		0x23
			MX8MQ_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x1f
			MX8MQ_IOMUXC_ENET_TXC_ENET1_RGMII_TXC		0x1f
			MX8MQ_IOMUXC_ENET_TD0_ENET1_RGMII_TD0		0x1f
			MX8MQ_IOMUXC_ENET_TD1_ENET1_RGMII_TD1		0x1f
			MX8MQ_IOMUXC_ENET_TD2_ENET1_RGMII_TD2		0x1f
			MX8MQ_IOMUXC_ENET_TD3_ENET1_RGMII_TD3		0x1f
			MX8MQ_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x91
			MX8MQ_IOMUXC_ENET_RXC_ENET1_RGMII_RXC		0x91
			MX8MQ_IOMUXC_ENET_RD0_ENET1_RGMII_RD0		0x91
			MX8MQ_IOMUXC_ENET_RD1_ENET1_RGMII_RD1		0x91
			MX8MQ_IOMUXC_ENET_RD2_ENET1_RGMII_RD2		0x91
			MX8MQ_IOMUXC_ENET_RD3_ENET1_RGMII_RD3		0x91
#define GP_FEC1_RESET	<&gpio1 9 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_GPIO1_IO09_GPIO1_IO9		0x19
#define GPIRQ_FEC1_PHY	<&gpio1 11 IRQ_TYPE_LEVEL_LOW>
			MX8MQ_IOMUXC_GPIO1_IO11_GPIO1_IO11		0x59
		>;
	};

	pinctrl_gpio_keys: gpio-keysgrp {
		fsl,pins = <
#define GP_GPIOKEY_POWER	<&gpio1 7 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_GPIO1_IO07_GPIO1_IO7		0x19	/* Pin 139 */
		>;
	};

	pinctrl_hog: hoggrp {
		fsl,pins = <
			/* J1 connector, odd */
			MX8MQ_IOMUXC_GPIO1_IO10_GPIO1_IO10		0x19	/* Pin 105 */
			MX8MQ_IOMUXC_GPIO1_IO05_GPIO1_IO5		0x19	/* Pin 143 */
			MX8MQ_IOMUXC_GPIO1_IO03_GPIO1_IO3		0x19	/* Pin 145 */
			MX8MQ_IOMUXC_NAND_CE3_B_GPIO3_IO4		0x19	/* Pin 149 */
			MX8MQ_IOMUXC_NAND_READY_B_GPIO3_IO16		0x19	/* Pin 153 */
			MX8MQ_IOMUXC_NAND_DATA05_GPIO3_IO11		0x19	/* Pin 155 */
			MX8MQ_IOMUXC_NAND_WP_B_GPIO3_IO18		0x19	/* Pin 157 */

			/* J1 connector, even */
			MX8MQ_IOMUXC_NAND_DATA04_GPIO3_IO10		0x19	/* Pin 82 */
			MX8MQ_IOMUXC_NAND_DATA03_GPIO3_IO9		0x19	/* Pin 84 */
			MX8MQ_IOMUXC_NAND_DATA02_GPIO3_IO8		0x19	/* Pin 86 */
			MX8MQ_IOMUXC_NAND_DATA01_GPIO3_IO7		0x19	/* Pin 88 */
			MX8MQ_IOMUXC_NAND_DATA00_GPIO3_IO6		0x19	/* Pin 90 */
			MX8MQ_IOMUXC_NAND_CE0_B_GPIO3_IO1		0x19	/* Pin 92 */
			MX8MQ_IOMUXC_NAND_ALE_GPIO3_IO0			0x19	/* Pin 96 */
			MX8MQ_IOMUXC_SAI1_TXD1_GPIO4_IO13		0x19	/* Pin 132 */
			MX8MQ_IOMUXC_SAI1_TXD2_GPIO4_IO14		0x19	/* Pin 134 */
			MX8MQ_IOMUXC_SAI1_TXD3_GPIO4_IO15		0x19	/* Pin 136 */
			MX8MQ_IOMUXC_SAI1_TXD4_GPIO4_IO16		0x19	/* Pin 138 */
			MX8MQ_IOMUXC_SAI1_TXD5_GPIO4_IO17		0x19	/* Pin 140 */
			MX8MQ_IOMUXC_SAI1_TXD6_GPIO4_IO18		0x19	/* Pin 142 */
			MX8MQ_IOMUXC_SAI1_TXD7_GPIO4_IO19		0x19	/* Pin 144 */
			MX8MQ_IOMUXC_SAI1_RXD1_GPIO4_IO3		0x19	/* Pin 146 */
			MX8MQ_IOMUXC_SAI1_RXD2_GPIO4_IO4		0x19	/* Pin 148 */
			MX8MQ_IOMUXC_SAI1_RXD3_GPIO4_IO5		0x19	/* Pin 150 */
			MX8MQ_IOMUXC_SAI1_RXD4_GPIO4_IO6		0x19	/* Pin 152 */
			MX8MQ_IOMUXC_SAI1_RXD5_GPIO4_IO7		0x19	/* Pin 154 */
			MX8MQ_IOMUXC_SAI1_RXD6_GPIO4_IO8		0x19	/* Pin 156 */
			MX8MQ_IOMUXC_SAI1_RXD7_GPIO4_IO9		0x19	/* Pin 158 */
			MX8MQ_IOMUXC_SAI1_RXC_GPIO4_IO1			0x19	/* Pin 160 */
			MX8MQ_IOMUXC_SAI1_RXFS_GPIO4_IO0		0x19	/* Pin 162 */
			MX8MQ_IOMUXC_SAI3_RXFS_GPIO4_IO28		0x19	/* Pin 198 */
			MX8MQ_IOMUXC_SAI3_RXC_GPIO4_IO29		0x19	/* Pin 200 */

			/* J13 Pin 2, BT_FUNC5 (TiWI only) */
			MX8MQ_IOMUXC_SAI5_RXD2_GPIO3_IO23		0xd6
			/* J13 Pin 4, WL_IRQ, not needed for Silex */
			MX8MQ_IOMUXC_SAI5_RXD0_GPIO3_IO21		0xd6
			/* J13 pin 9, unused */
			MX8MQ_IOMUXC_SD2_CD_B_GPIO2_IO12		0x19
			/* J13 Pin 41, BT_CLK_REQ */
			MX8MQ_IOMUXC_SAI5_RXD1_GPIO3_IO22		0xd6
			/* J13 Pin 42, BT_HOST_WAKE */
			MX8MQ_IOMUXC_SAI5_MCLK_GPIO3_IO25		0xd6
			MX8MQ_IOMUXC_SPDIF_EXT_CLK_GPIO5_IO5		0x19	/* TP79 */
			MX8MQ_IOMUXC_GPIO1_IO08_GPIO1_IO8		0x49	/* TP80 */

			/* Clock for both CSI1 and CSI2 */
			MX8MQ_IOMUXC_GPIO1_IO15_CCMSRCGPCMIX_CLKO2	0x07
		>;
	};

	pinctrl_i2c1: i2c1grp {
		fsl,pins = <
			MX8MQ_IOMUXC_I2C1_SCL_I2C1_SCL			0x4000007f
			MX8MQ_IOMUXC_I2C1_SDA_I2C1_SDA			0x4000007f
		>;
	};

	pinctrl_i2c1_pca9546: i2c1-pca9546grp {
		fsl,pins = <
#define GP_I2C1_PCA9546_RESET	<&gpio1 4 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_GPIO1_IO04_GPIO1_IO4		0x49
		>;
	};

	pinctrl_i2c2: i2c2grp {
		fsl,pins = <
			MX8MQ_IOMUXC_I2C2_SCL_I2C2_SCL			0x4000007f
			MX8MQ_IOMUXC_I2C2_SDA_I2C2_SDA			0x4000007f
		>;
	};

	pinctrl_i2c2_csi1: i2c2-csi1grp {
		fsl,pins = <
#define GP_CSI1_MIPI_PWDN	<&gpio3 3 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_CE2_B_GPIO3_IO3		0x61
#define GP_CSI1_MIPI_RESET	<&gpio3 17 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_WE_B_GPIO3_IO17		0x61
		>;
	};

	pinctrl_i2c3: i2c3grp {
		fsl,pins = <
			MX8MQ_IOMUXC_I2C3_SCL_I2C3_SCL			0x4000007f
			MX8MQ_IOMUXC_I2C3_SDA_I2C3_SDA			0x4000007f
		>;
	};

	pinctrl_i2c3_csi2: i2c3-csi2grp {
		fsl,pins = <
#define GP_CSI2_MIPI_PWDN	<&gpio3 2 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_CE1_B_GPIO3_IO2		0x61
#define GP_CSI2_MIPI_RESET	<&gpio2 19 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_SD2_RESET_B_GPIO2_IO19		0x61
		>;
	};

	pinctrl_i2c4: i2c4grp {
		fsl,pins = <
			MX8MQ_IOMUXC_I2C4_SCL_I2C4_SCL			0x4000007f
			MX8MQ_IOMUXC_I2C4_SDA_I2C4_SDA			0x4000007f
		>;
	};

	pinctrl_i2c4_gt911: i2c4-gt911grp {
		fsl,pins = <
#define GPIRQ_GT911 		<&gpio3 12 IRQ_TYPE_LEVEL_HIGH>
#define GP_GT911_IRQ 		<&gpio3 12 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_DATA06_GPIO3_IO12		0xd6
			/* driver writes levels, instead of active/inactive */
#define GP_GT911_RESET		<&gpio3 13 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_DATA07_GPIO3_IO13		0x49
		>;
	};

	pinctrl_i2c4_ft5x06: i2c4-ft5x06grp {
		fsl,pins = <
#define GPIRQ_I2C4_FT5X06	<&gpio3 12 IRQ_TYPE_EDGE_FALLING>
#define GP_I2C4_FT5X06_WAKE	<&gpio3 12 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_DATA06_GPIO3_IO12		0x49
#define GP_I2C4_FT5X06_RESET	<&gpio3 13 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_DATA07_GPIO3_IO13		0x49
		>;
	};

	pinctrl_i2c4_st1633: i2c4-st1633grp {
		fsl,pins = <
#define GPIRQ_ST1633 		<&gpio3 12 IRQ_TYPE_EDGE_FALLING>
#define GP_ST1633_IRQ 		<&gpio3 12 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_DATA06_GPIO3_IO12		0xd6
#define GP_ST1633_RESET		<&gpio3 13 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_DATA07_GPIO3_IO13		0x49
		>;
	};

	pinctrl_i2c4_sn65dsi83: i2c4-sn65dsi83grp {
		fsl,pins = <
#define GPIRQ_I2C4_SN65DSI83	<&gpio1 1 IRQ_TYPE_LEVEL_HIGH>
			MX8MQ_IOMUXC_GPIO1_IO01_GPIO1_IO1		0x04
#define GP_I2C4_SN65DSI83_EN	<&gpio3 15 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_RE_B_GPIO3_IO15		0x06
		>;
	};

	pinctrl_i2c4_pca9546: i2c4-pca9546grp {
		fsl,pins = <
#define GP_I2C4_PCA9546_RESET	<&gpio3 5 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_CLE_GPIO3_IO5			0x49	/* Pin 151 */
		>;
	};

	pinctrl_i2c4b_wm8960: i2c4b-wm8960grp {
		fsl,pins = <
#define GP_WM8960_HP_DET		<&gpio3 14 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_DQS_GPIO3_IO14		0x19	/* Pin 53 */
		>;
	};

	pinctrl_i2c4d_rv4162: i2c4d-rv4162grp {
		fsl,pins = <
#define GPIRQ_RV4162		<&gpio1 6 IRQ_TYPE_LEVEL_LOW>
			MX8MQ_IOMUXC_GPIO1_IO06_GPIO1_IO6		0x49
		>;
	};

	pinctrl_lcm_jm430: lcm-jm430grp {
		fsl,pins = <
#define GP_TC358762_EN	<&gpio3 15 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_NAND_RE_B_GPIO3_IO15		0x16
		>;
	};

	pinctrl_ltk0680ytmdb: ltk0680ytmdbgrp {
		fsl,pins = <
#define GP_MIPI_RESET	<&gpio1 1 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_NAND_RE_B_GPIO3_IO15		0x16
		>;
	};

	pinctrl_ltk080a60a004t: ltk080a60a004tgrp {
		fsl,pins = <
#define GP_LTK08_MIPI_EN	<&gpio1 1 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_GPIO1_IO01_GPIO1_IO1		0x16
		>;
	};

	pinctrl_pcie0: pcie0grp {
		fsl,pins = <
#define GP_PCIE0_RESET		<&gpio5 7 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_ECSPI1_MOSI_GPIO5_IO7		0x16
#define GP_PCIE0_DISABLE	<&gpio5 6 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_ECSPI1_SCLK_GPIO5_IO6		0x16
		>;
	};

	pinctrl_pwm2: pwm2grp {
		fsl,pins = <
			MX8MQ_IOMUXC_SPDIF_RX_PWM2_OUT			0x16
		>;
	};

	pinctrl_pwm3: pwm3grp {
		fsl,pins = <
			MX8MQ_IOMUXC_SPDIF_TX_PWM3_OUT			0x16
		>;
	};

	pinctrl_pwm4: pwm4grp {
		fsl,pins = <
			MX8MQ_IOMUXC_SAI3_MCLK_PWM4_OUT			0x16
		>;
	};

	pinctrl_reg_arm_dram: reg-arm-dram {
		fsl,pins = <
#define GP_ARM_DRAM_VSEL	<&gpio3 24 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_SAI5_RXD3_GPIO3_IO24		0x16
		>;
	};

	pinctrl_reg_dram_1p1v: reg-dram-1p1v {
		fsl,pins = <
#define GP_DRAM_1P1_VSEL	<&gpio2 11 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_SD1_STROBE_GPIO2_IO11		0x16
		>;
	};

	pinctrl_reg_soc_gpu_vpu: reg-soc-gpu-vpu {
		fsl,pins = <
#define GP_SOC_GPU_VPU_VSEL	<&gpio2 20 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_SD2_WP_GPIO2_IO20			0x16
		>;
	};

	pinctrl_reg_usbotg_vbus: reg-usbotg-vbusgrp {
		fsl,pins = <
#define GP_REG_USB_OTG_VBUS	<&gpio1 12 GPIO_ACTIVE_HIGH>
			MX8MQ_IOMUXC_GPIO1_IO12_GPIO1_IO12		0x16
		>;
	};

	pinctrl_sai1: sai1grp {
		fsl,pins = <
			/* wm8960 */
			MX8MQ_IOMUXC_SAI1_MCLK_SAI1_MCLK		0xd6
			MX8MQ_IOMUXC_SAI1_TXFS_SAI1_TX_SYNC		0xd6
			MX8MQ_IOMUXC_SAI1_TXC_SAI1_TX_BCLK		0xd6
			MX8MQ_IOMUXC_SAI1_TXD0_SAI1_TX_DATA0		0xd6
			MX8MQ_IOMUXC_SAI1_RXD0_SAI1_RX_DATA0		0xd6
		>;
	};

	pinctrl_sai2: sai2grp {
		fsl,pins = <
			/* our carrier, unused */
			MX8MQ_IOMUXC_SAI2_RXD0_SAI2_RX_DATA0		0xd6	/* Pin 166 */
			MX8MQ_IOMUXC_SAI2_RXFS_SAI2_RX_SYNC		0xd6	/* Pin 168 */
			MX8MQ_IOMUXC_SAI2_TXC_SAI2_TX_BCLK		0xd6	/* Pin 170 */
			MX8MQ_IOMUXC_SAI2_TXFS_SAI2_TX_SYNC		0xd6	/* Pin 172 */
			MX8MQ_IOMUXC_SAI2_RXC_SAI2_RX_BCLK		0xd6	/* Pin 174 */
			MX8MQ_IOMUXC_SAI2_MCLK_SAI2_MCLK		0xd6	/* Pin 176 */
			MX8MQ_IOMUXC_SAI2_TXD0_SAI2_TX_DATA0		0xd6	/* Pin 168 */
		>;
	};

	pinctrl_sai3: sai3grp {
		fsl,pins = <
			/* Bluetooth PCM */
			MX8MQ_IOMUXC_SAI3_TXFS_SAI3_TX_SYNC		0xd6
			MX8MQ_IOMUXC_SAI3_TXC_SAI3_TX_BCLK		0xd6
			MX8MQ_IOMUXC_SAI3_TXD_SAI3_TX_DATA0		0xd6
			MX8MQ_IOMUXC_SAI3_RXD_SAI3_RX_DATA0		0xd6
		>;
	};

	pinctrl_uart1: uart1grp {
		fsl,pins = <
			MX8MQ_IOMUXC_UART1_RXD_UART1_DCE_RX		0x45
			MX8MQ_IOMUXC_UART1_TXD_UART1_DCE_TX		0x45
		>;
	};

	pinctrl_uart2: uart2grp {
		fsl,pins = <
			MX8MQ_IOMUXC_UART2_RXD_UART2_DCE_RX		0x45
			MX8MQ_IOMUXC_UART2_TXD_UART2_DCE_TX		0x45
		>;
	};

	pinctrl_uart3: uart3grp {
		fsl,pins = <
			MX8MQ_IOMUXC_UART3_RXD_UART3_DCE_RX		0x45
			MX8MQ_IOMUXC_UART3_TXD_UART3_DCE_TX		0x45
			MX8MQ_IOMUXC_ECSPI1_SS0_UART3_DCE_RTS_B		0x45
			MX8MQ_IOMUXC_ECSPI1_MISO_UART3_DCE_CTS_B	0x45
		>;
	};

	pinctrl_uart4: uart4grp {
		fsl,pins = <
			MX8MQ_IOMUXC_UART4_RXD_UART4_DCE_RX		0x45
			MX8MQ_IOMUXC_UART4_TXD_UART4_DCE_TX		0x45
		>;
	};

	pinctrl_usb3_0: usb3-0grp {
		fsl,pins = <
			MX8MQ_IOMUXC_GPIO1_IO13_USB1_OTG_OC		0x16
		>;
	};

	pinctrl_usb3_1: usb3-1grp {
		fsl,pins = <
#define GP_USB3_1_HUB_RESET	<&gpio1 14 GPIO_ACTIVE_LOW>
			MX8MQ_IOMUXC_GPIO1_IO14_GPIO1_IO14		0x16
		>;
	};

	pinctrl_usdhc1: usdhc1grp {
		fsl,pins = <
			MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x83
			MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xc3
			MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xc3
			MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xc3
			MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xc3
			MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xc3
			MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xc3
			MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xc3
			MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xc3
			MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xc3
		>;
	};

	pinctrl_usdhc1_100mhz: usdhc1grp100mhz {
		fsl,pins = <
			MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x8d
			MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xcd
			MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xcd
			MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xcd
			MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xcd
			MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xcd
			MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xcd
			MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xcd
			MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xcd
			MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xcd
		>;
	};

	pinctrl_usdhc1_200mhz: usdhc1grp200mhz {
		fsl,pins = <
			MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x9f
			MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xdf
			MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xdf
			MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xdf
			MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xdf
			MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xdf
			MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xdf
			MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xdf
			MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xdf
			MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xdf
		>;
	};


	pinctrl_usdhc2: usdhc2grp {
		fsl,pins = <
			MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x03
			MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xc3
			MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xc3
			MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xc3
			MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xc3
			MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xc3
		>;
	};

	pinctrl_usdhc2_100mhz: usdhc2grp100mhz {
		fsl,pins = <
			MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x0d
			MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xcd
			MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xcd
			MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xcd
			MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xcd
			MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xcd
		>;
	};

	pinctrl_usdhc2_200mhz: usdhc2grp200mhz {
		fsl,pins = <
			MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x1e
			MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xce
			MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xce
			MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xce
			MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xce
			MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xce
		>;
	};

	pinctrl_wdog: wdoggrp {
		fsl,pins = <
			MX8MQ_IOMUXC_GPIO1_IO02_WDOG1_WDOG_B		0xc6
		>;
	};
};

/ {
	model = "Boundary Devices i.MX8MQ Nitrogen8M_som";
	compatible = "boundary,imx8mq-nitrogen8m_som", "fsl,imx8mq";

#if 0
	aliases {
		backlight_mipi = &backlight_mipi;
		dcss = &dcss;
		fb_mipi = &fb_mipi;
		fb_hdmi = &hdmi;
		lcdif = &lcdif;
		mipi = &fb_mipi;
		mipi_cmds_lcm_jm430 = &mipi_cmds_lcm_jm430;
		mipi_cmds_ltk0680ytmdb = &mipi_cmds_ltk0680ytmdb;
		mipi_cmds_ltk080a60a004t = &mipi_cmds_ltk080a60a004t;
		mipi_cmds_m101nwwb = &mipi_cmds_ltk080a60a004t;		/* Same commands work for both */
		mipi_dsi = &mipi_dsi;
		mipi_dsi_bridge = &mipi_dsi_bridge;
		mipi_dsi_phy = &mipi_dsi_phy;
		mipi_to_lvds = &mipi_to_lvds;
		pinctrl_lcm_jm430 = &pinctrl_lcm_jm430;
		pinctrl_ltk0680ytmdb = &pinctrl_ltk0680ytmdb;
		pinctrl_ltk080a60a004t = &pinctrl_ltk080a60a004t;
		sound_hdmi = &sound_hdmi;
		t_mipi = &t_mipi;
	};

	alias_create_phandles {
		p1 = <&mipi_cmds_lcm_jm430>;
		p2 = <&mipi_cmds_ltk0680ytmdb>;
		p3 = <&mipi_cmds_ltk080a60a004t>;
		p4 = <&mipi_to_lvds>;
		p5 = <&pinctrl_lcm_jm430>;
		p6 = <&pinctrl_ltk0680ytmdb>;
		p7 = <&pinctrl_ltk080a60a004t>;
	};

	backlight_mipi: backlight-mipi {
		brightness-levels = <0 1 2 3 4 5 6 7 8 9 10>;
		compatible = "pwm-backlight";
		default-brightness-level = <8>;
		display = <&lcdif>;
		pwms = <&pwm3 0 30000>;		/* 33.3 Khz */
		status = "disabled";
	};
#endif

	bt-rfkill {
		compatible = "net,rfkill-gpio";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_bt_rfkill>;
		name = "bt-rfkill";
		type = <2>; /* Bluetooth */
		reset-gpios = GP_BT_RFKILL_RESET;
		status = "okay";
	};

	gpio-keys {
		compatible = "gpio-keys";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_gpio_keys>;

		power {
			label = "Power Button";
			gpios = GP_GPIOKEY_POWER;
			linux,code = <KEY_POWER>;
			gpio-key,wakeup;
		};
	};

	reg_usb_otg_vbus: regulator-usb-otg-vbus {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_usbotg_vbus>;
		regulator-name = "usb_otg_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = GP_REG_USB_OTG_VBUS;
		enable-active-high;
	};

	reg_vref_0v9: regulator-vref-0v9 {
		compatible = "regulator-fixed";
		regulator-name = "vref-0v9";
		regulator-min-microvolt = <900000>;
		regulator-max-microvolt = <900000>;
	};

	reg_vref_1v8: regulator-vref-1v8 {
		compatible = "regulator-fixed";
		regulator-name = "vref-1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	reg_vref_2v5: regulator-vref-2v5 {
		compatible = "regulator-fixed";
		regulator-name = "vref-2v5";
		regulator-min-microvolt = <2500000>;
		regulator-max-microvolt = <2500000>;
	};

	reg_vref_3v3: regulator-vref-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "vref-3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	reg_vref_5v: regulator-vref-5v {
		compatible = "regulator-fixed";
		regulator-name = "vref-5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

#if 0
	sound-wm8960 {
		compatible = "fsl,imx-audio-wm8960";
		model = "wm8960-audio";
		cpu-dai = <&sai1>;
		codec-master;
		audio-codec = <&wm8960>;
		audio-routing =
			"Headphone Jack", "HP_L",
			"Headphone Jack", "HP_R",
			"Ext Spk", "SPK_LP",
			"Ext Spk", "SPK_LN",
			"Ext Spk", "SPK_RP",
			"Ext Spk", "SPK_RN",
			"LINPUT1", "Main MIC",
			"Main MIC", "MICB";
		/* JD2: hp detect high for headphone*/
		hp-det = <2 0>;
		hp-det-gpios = GP_WM8960_HP_DET;
	};

	sound_hdmi: sound-hdmi {
		compatible = "fsl,imx-audio-cdnhdmi";
		model = "imx-audio-hdmi";
		audio-cpu = <&sai4>;
		constraint-rate = <32000 44100 48000 96000 192000>;
		protocol = <1>;
		status = "okay";
	};
#endif
};

&A53_0 {
	operating-points = <
		/* kHz    uV */
		1500000 1000000
		1300000 1000000
		1000000 900000
		800000  900000
	>;
};

&clk {
	assigned-clocks = <&clk IMX8MQ_AUDIO_PLL1>, <&clk IMX8MQ_AUDIO_PLL2>;
	assigned-clock-rates = <786432000>, <722534400>;
};

#if 0
&csi1_bridge {
	fsl,mipi-mode;
	fsl,two-8bit-sensor-mode;
	status = "okay";

	port {
		csi1_ep: endpoint {
			remote-endpoint = <&csi1_mipi_ep>;
		};
	};
};

&csi2_bridge {
	fsl,mipi-mode;
	fsl,two-8bit-sensor-mode;
	status = "okay";

	port {
		csi2_ep: endpoint {
			remote-endpoint = <&csi2_mipi_ep>;
		};
	};
};

&dcss {
	status = "okay";
	disp-dev = "hdmi_disp";
};

&ecspi2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_ecspi2>;
	fsl,spi-num-chipselects = <1>;
	cs-gpios = GP_ECSPI2_CS0;
	status = "disabled";
	#address-cells = <1>;
	#size-cells = <0>;

	spidev@0 {
		compatible = "spidev";
		spi-max-frequency = <2000000>;
		reg = <0>;
	};
};
#endif

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_fec1>;
#if 0
	phy-reset-gpios = GP_FEC1_RESET;
#endif
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	fsl,magic-packet;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@4 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <4>;
			interrupts-extended = GPIRQ_FEC1_PHY;
		};
	};
};

#if 0
&gpu {
#if 0
	clocks = <&clk IMX8MQ_CLK_GPU_ROOT>,
		 <&clk IMX8MQ_CLK_GPU_SHADER_DIV>,
		 <&clk IMX8MQ_CLK_GPU_AXI_DIV>,
		 <&clk IMX8MQ_CLK_GPU_AHB_DIV>;
	clock-names = "gpu3d_clk", "gpu3d_shader_clk", "gpu3d_axi_clk", "gpu3d_ahb_clk";
	assigned-clocks = <&clk IMX8MQ_CLK_GPU_CORE_SRC>,
			  <&clk IMX8MQ_CLK_GPU_SHADER_SRC>,
			  <&clk IMX8MQ_CLK_GPU_AXI_SRC>,
			  <&clk IMX8MQ_CLK_GPU_AHB_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_GPU_PLL_OUT>,
				 <&clk IMX8MQ_SYS2_PLL_1000M>,
				 <&clk IMX8MQ_GPU_PLL_OUT>,
				 <&clk IMX8MQ_GPU_PLL_OUT>;
	assigned-clock-rates =  <800000000>,
				<1000000000>,
				<800000000>,
				<800000000>;
#endif
	status = "okay";
};

&hdmi {
	status = "okay";
};
#endif

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";

	i2cmux@70 {
		compatible = "pca9546";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c1_pca9546>;
		reg = <0x70>;
		reset-gpios = GP_I2C1_PCA9546_RESET;
		#address-cells = <1>;
		#size-cells = <0>;

		i2c1a: i2c1@0 {
			reg = <0>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c1b: i2c1@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c1c: i2c1@2 {
			reg = <2>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c1d: i2c1@3 {
			reg = <3>;
			#address-cells = <1>;
			#size-cells = <0>;
		};
	};
};

&i2c1a {
	reg_arm_dram: fan53555@60 {
		compatible = "fcs,fan53555";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_arm_dram>;
		reg = <0x60>;
		regulator-min-microvolt =  <900000>;
		regulator-max-microvolt = <1000000>;
		regulator-always-on;
		vsel-gpios = GP_ARM_DRAM_VSEL;
	};
};

&i2c1b {
	reg_dram_1p1v: fan53555@60 {
		compatible = "fcs,fan53555";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_dram_1p1v>;
		reg = <0x60>;
		regulator-min-microvolt = <1100000>;
		regulator-max-microvolt = <1100000>;
		regulator-always-on;
		vsel-gpios = GP_DRAM_1P1_VSEL;
	};
};

&i2c1c {
	reg_soc_gpu_vpu: fan53555@60 {
		compatible = "fcs,fan53555";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_soc_gpu_vpu>;
		reg = <0x60>;
		regulator-min-microvolt =  <900000>;
		regulator-max-microvolt = <1000000>;
		regulator-always-on;
		vsel-gpios = GP_SOC_GPU_VPU_VSEL;
	};
};

&i2c1d {
};

&i2c2 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c2>;
	status = "okay";

#if 0
	ov5640-mipi1@3c {
		compatible = "ov5640_mipisubdev";
		reg = <0x3c>;
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c2_csi1>;
		clocks = <&clk IMX8MQ_CLK_CLKO2_DIV>;
		clock-names = "csi_mclk";
		assigned-clocks = <&clk IMX8MQ_CLK_CLKO2_SRC>,
				  <&clk IMX8MQ_CLK_CLKO2_DIV>;
		assigned-clock-parents = <&clk IMX8MQ_SYS2_PLL_200M>;
		assigned-clock-rates = <0>, <25000000>;
		csi_id = <0>;
		AVDD-supply = <&reg_vref_2v5>;
		DVDD-supply = <&reg_vref_3v3>;
		DOVDD-supply = <&reg_vref_1v8>;
		pwn-gpios = GP_CSI1_MIPI_PWDN;
		rst-gpios = GP_CSI1_MIPI_RESET;
		mclk = <25000000>;
		mipi_csi;

		port {
			ov5640_mipi1_ep: endpoint {
				remote-endpoint = <&mipi1_sensor_ep>;
			};
		};
	};
#endif

	pcie-clock@6a {
		compatible = "idt,9FGV0241AKILF";
		/* TODO */
		reg = <0x6a>;
	};
};

&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c3>;
	status = "okay";

#if 0
	ov5640-mipi2@3c {
		compatible = "ov5640_mipisubdev";
		reg = <0x3c>;
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c3_csi2>;
		clocks = <&clk IMX8MQ_CLK_CLKO2_DIV>;
		clock-names = "csi_mclk";
		assigned-clocks = <&clk IMX8MQ_CLK_CLKO2_SRC>,
				  <&clk IMX8MQ_CLK_CLKO2_DIV>;
		assigned-clock-parents = <&clk IMX8MQ_SYS2_PLL_200M>;
		assigned-clock-rates = <0>, <25000000>;
		csi_id = <1>;
		AVDD-supply = <&reg_vref_2v5>;
		DVDD-supply = <&reg_vref_3v3>;
		DOVDD-supply = <&reg_vref_1v8>;
		pwn-gpios = GP_CSI2_MIPI_PWDN;
		rst-gpios = GP_CSI2_MIPI_RESET;
		mclk = <25000000>;
		mipi_csi;

		port {
			ov5640_mipi2_ep: endpoint {
				remote-endpoint = <&mipi2_sensor_ep>;
			};
		};
	};
#endif
};

&i2c4 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c4>;
	status = "okay";

	touchscreen@5d {
		compatible = "goodix,gt9271";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4_gt911>;
		reg = <0x5d>;
		esd-recovery-timeout-ms = <2000>;
		interrupts-extended = GPIRQ_GT911;
		irq-gpios = GP_GT911_IRQ;
		reset-gpios = GP_GT911_RESET;
	};

#if 0
	mipi_to_lvds: mipi-to-lvds@2c {
		clocks = <&mipi_dsi_phy 0>;
		clock-names = "mipi_clk";
		compatible = "ti,sn65dsi83";
		display = <&lcdif>;
		display-dsi = <&fb_mipi>;
		enable-gpios = GP_I2C4_SN65DSI83_EN;
		interrupts-extended = GPIRQ_I2C4_SN65DSI83;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4_sn65dsi83>;
		reg = <0x2c>;
		status = "disabled";
	};
#endif

	touchscreen@38 {
		compatible = "ft5x06-ts";
		interrupts-extended = GPIRQ_I2C4_FT5X06;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4_ft5x06>;
		reg = <0x38>;
		wakeup-gpios = GP_I2C4_FT5X06_WAKE;
		reset-gpios = GP_I2C4_FT5X06_RESET;
	};

	touchscreen@55 {
		compatible = "sitronix,st1633i";
		reg = <0x55>;
		interrupts-extended = GPIRQ_ST1633;
		/* pins used by touchscreen */
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4_st1633>;
		reset-gpios = GP_ST1633_RESET;

		wakeup-gpios = GP_ST1633_IRQ;
	};

	i2cmux@70 {
		compatible = "pca9546";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4_pca9546>;
		reg = <0x70>;
		reset-gpios = GP_I2C4_PCA9546_RESET;
		#address-cells = <1>;
		#size-cells = <0>;

		i2c4a: i2c4@0 {
			reg = <0>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c4b: i2c4@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c4c: i2c4@2 {
			reg = <2>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c4d: i2c4@3 {
			reg = <3>;
			#address-cells = <1>;
			#size-cells = <0>;
		};
	};
};

&i2c4a {
	/* pciei */
};

&i2c4b {
	wm8960: codec@1a {
		compatible = "wlf,wm8960";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4b_wm8960>;
		reg = <0x1a>;
		clocks = <&clk IMX8MQ_CLK_SAI1_ROOT>;
		clock-names = "mclk";
		wlf,shared-lrclk;
	};
};

&i2c4c {
	/* unused */
};

&i2c4d {
	rtc@68 {
		compatible = "microcrystal,rv4162";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c4d_rv4162>;
		reg = <0x68>;
		interrupts-extended = GPIRQ_RV4162;
		wakeup-source;
	};
};

#if 0
&lcdif {
	status = "disabled";
	assigned-clocks = <&clk IMX8MQ_CLK_LCDIF_PIXEL_SRC>,
			  <&clk IMX8MQ_VIDEO_PLL1_REF_SEL>;
	assigned-clock-parents = <&clk IMX8MQ_VIDEO_PLL1_OUT>,
				 <&clk IMX8MQ_CLK_25M>;
	max-res = <1920>, <1920>;

	port@0 {
		lcdif_mipi_dsi: mipi-dsi-endpoint {
			remote-endpoint = <&mipi_dsi_in>;
		};
	};
};

&mipi_csi_1 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi1_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi1_ep>;
			data-lanes = <1 2>;
		};

		csi1_mipi_ep: endpoint2 {
			remote-endpoint = <&csi1_ep>;
		};
	};
};

&mipi_csi_2 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi2_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi2_ep>;
			data-lanes = <1 2>;
		};

		csi2_mipi_ep: endpoint2 {
			remote-endpoint = <&csi2_ep>;
		};
	};
};

&mipi_dsi_phy {
	status = "disabled";
};

&mipi_dsi {
	status = "disabled";
	as_bridge;
	assigned-clocks = <&clk IMX8MQ_CLK_DSI_CORE_SRC>,
			  <&clk IMX8MQ_CLK_DSI_PHY_REF_SRC>,
			  <&clk IMX8MQ_VIDEO_PLL1_REF_SEL>;
	assigned-clock-parents = <&clk IMX8MQ_SYS1_PLL_266M>,
				 <&clk IMX8MQ_VIDEO_PLL1_OUT>,
				 <&clk IMX8MQ_CLK_25M>;
	assigned-clock-rates = <266000000>;

	clocks = <&clk IMX8MQ_CLK_DSI_CORE_DIV>,
		 <&clk IMX8MQ_CLK_DSI_PHY_REF_DIV>,
		 <&clk IMX8MQ_VIDEO_PLL1>;
	clock-names = "core", "phy_ref", "pixel_pll";

	port@1 {
		mipi_dsi_in: endpoint {
			remote-endpoint = <&lcdif_mipi_dsi>;
		};
	};
};

&mipi_dsi_bridge {
	status = "disabled";

	fb_mipi: panel@0 {
		bits-per-color = <8>;
		bridge-de-active = <0>;
		bridge-sync-active = <1>;
		bus-format = "rgb888";
		compatible = "panel,simple";
		dsi-format = "rgb888";
		dsi-lanes = <4>;
		mode-skip-eot;
		mode-video;
		mode-video-burst;
		panel-height-mm = <136>;
		panel-width-mm = <217>;
		power-supply = <&reg_vref_5v>;
		reg = <0>;
		spwg;

		display-timings {
			t_mipi: t-dsi-default {
				/* m101nwwb by default */
				clock-frequency = <70000000>;
				hactive = <1280>;
				vactive = <800>;
				hback-porch = <5>;
				hfront-porch = <123>;
				vback-porch = <3>;
				vfront-porch = <24>;
				hsync-len = <1>;
				vsync-len = <1>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <1>;
			};
		};

		port {
			panel1_in: endpoint {
				remote-endpoint = <&mipi_dsi_bridge_out>;
			};
		};
	};

	port@1 {
		mipi_dsi_bridge_out: endpoint {
			remote-endpoint = <&panel1_in>;
		};
	};
};

&mu {
	status = "okay";
};

&pcie0 {
#if 1
	clocks = <&clk IMX8MQ_CLK_PCIE1_ROOT>,
		<&clk IMX8MQ_CLK_PCIE1_AUX_CG>,
		<&clk IMX8MQ_CLK_PCIE1_PHY_CG>,
		<&clk IMX8MQ_CLK_CLK2_CG>;
	clock-names = "pcie", "pcie_bus", "pcie_phy", "pcie_ext_src";
	ext_osc = <0>;
#else
	ext_osc = <1>;
#endif
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie0>;
	/* TODO check clock */
	disable-gpio = GP_PCIE0_DISABLE;
	reset-gpio = GP_PCIE0_RESET;
	status = "okay";
};

&pcie1 {
	/* TODO check clock */
	ext_osc = <1>;
	hard-wired = <1>;
	status = "disabled";
};
#endif

&pwm2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm2>;
	status = "okay";
};

#if 0
&pwm3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm3>;
	status = "okay";
};

&pwm4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm4>;
	status = "okay";
};

&sai1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai1>;
	assigned-clocks = <&clk IMX8MQ_CLK_SAI1_SRC>,
			<&clk IMX8MQ_CLK_SAI1_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <12288000>;
	status = "okay";
};

&sai2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai2>;
	status = "disabled";
};

&sai3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai3>;
	status = "okay";
};

&sai4 {
	assigned-clocks = <&clk IMX8MQ_CLK_SAI4_SRC>,
			<&clk IMX8MQ_CLK_SAI4_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <24576000>;
	clocks = <&clk IMX8MQ_CLK_SAI4_IPG>, <&clk IMX8MQ_CLK_DUMMY>,
		<&clk IMX8MQ_CLK_SAI4_ROOT>, <&clk IMX8MQ_CLK_DUMMY>,
		<&clk IMX8MQ_CLK_DUMMY>, <&clk IMX8MQ_AUDIO_PLL1_OUT>,
		<&clk IMX8MQ_AUDIO_PLL2_OUT>;
	clock-names = "bus", "mclk0", "mclk1", "mclk2", "mclk3", "pll8k", "pll11k";
	status = "okay";
};
#endif

#if 0
&uart1 { /* console */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART1_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};

&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART2_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};

&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART3_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_SYS1_PLL_80M>;
	uart-has-rtscts;
	status = "okay";
};

&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART4_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};
#endif

&usb3_phy0 {
	status = "okay";
};

&usb3_0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usb3_0>;
	status = "okay";
};

&usb_dwc3_0 {
#if 0
	status = "okay";
#endif
	dr_mode = "otg";
	vbus-supply = <&reg_usb_otg_vbus>;
};

&usb3_phy1 {
	status = "okay";
};

&usb3_1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usb3_1>;
	reset-gpios = GP_USB3_1_HUB_RESET;
	status = "okay";
};

&usb_dwc3_1 {
#if 0
	status = "okay";
#endif
	dr_mode = "host";
};

&usdhc1 {
	cap-mmc-highspeed;
	bus-width = <8>;
	// mmc-ddr-1_8v;
	// mmc-hs200-1_8v;
	fsl,strobe-dll-delay-target = <5>;
	fsl,tuning-start-tap = <63>;
	fsl,tuning-step = <2>;
	no-mmc-hs400;
	non-removable;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc1>;
	pinctrl-1 = <&pinctrl_usdhc1_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc1_200mhz>;
	status = "okay";
	vmmc-supply = <&reg_vref_1v8>;
	vqmmc-1-8-v;
};

&usdhc2 {
	bus-width = <4>;
	fsl,tuning-start-tap = <70>;
	fsl,tuning-step = <2>;
	no-sd-uhs-sdr104;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>;
	non-removable;
	status = "okay";
	tuning-delay = <32>;
	tuning-mode = <1>;
	vmmc-supply = <&reg_vref_3v3>;
};


&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,ext-reset-output;
	status = "okay";
};
