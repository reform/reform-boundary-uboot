/*
 * Copyright 2018 NXP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/dts-v1/;

#include "fsl-imx8mm.dtsi"

&iomuxc {
	iomuxc_pinctrl: iomuxc-pinctrlgrp {
	};
};

&iomuxc_pinctrl {
	pinctrl_bt_rfkill: bt-rfkillgrp {
		fsl,pins = <
#define GP_BT_RFKILL_RESET	<&gpio3 14 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_NAND_DQS_GPIO3_IO14		0x119
		>;
	};

	pinctrl_ecspi2: ecspi2grp {
		fsl,pins = <
			/* J15 */
#define GP_ECSPI2_CS0	<&gpio5 13 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_ECSPI2_SS0_GPIO5_IO13		0x19	/* Pin 39 */
			MX8MM_IOMUXC_ECSPI2_MISO_ECSPI2_MISO		0x19	/* Pin 41 */
			MX8MM_IOMUXC_ECSPI2_SCLK_ECSPI2_SCLK		0x19	/* Pin 43 */
			MX8MM_IOMUXC_ECSPI2_MOSI_ECSPI2_MOSI		0x19	/* Pin 45 */
		>;
	};

	pinctrl_fec1: fec1grp {
		fsl,pins = <
			MX8MM_IOMUXC_ENET_MDC_ENET1_MDC			0x00
			/* PAD_CTL_ODE is screwy on 8mm mini, avoid it */
			MX8MM_IOMUXC_ENET_MDIO_ENET1_MDIO		0x00
			MX8MM_IOMUXC_ENET_RD0_ENET1_RGMII_RD0		0x91
			MX8MM_IOMUXC_ENET_RD1_ENET1_RGMII_RD1		0x91
			MX8MM_IOMUXC_ENET_RD2_ENET1_RGMII_RD2		0x91
			MX8MM_IOMUXC_ENET_RD3_ENET1_RGMII_RD3		0x91
			MX8MM_IOMUXC_ENET_RXC_ENET1_RGMII_RXC		0x91
			MX8MM_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x91
			MX8MM_IOMUXC_ENET_TD0_ENET1_RGMII_TD0		0x1f
			MX8MM_IOMUXC_ENET_TD1_ENET1_RGMII_TD1		0x1f
			MX8MM_IOMUXC_ENET_TD2_ENET1_RGMII_TD2		0x1f
			MX8MM_IOMUXC_ENET_TD3_ENET1_RGMII_TD3		0x1f
			MX8MM_IOMUXC_ENET_TXC_ENET1_RGMII_TXC		0x1f
			MX8MM_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x1f
#define GP_FEC1_RESET	<&gpio3 15 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_NAND_RE_B_GPIO3_IO15		0x159
#define GPIRQ_FEC1_PHY	<&gpio3 16 IRQ_TYPE_LEVEL_LOW>
			MX8MM_IOMUXC_NAND_READY_B_GPIO3_IO16		0x159
		>;
	};

	pinctrl_flexspi: flexspigrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_ALE_QSPI_A_SCLK		0x1c2
			MX8MM_IOMUXC_NAND_CE0_B_QSPI_A_SS0_B		0x82
			MX8MM_IOMUXC_NAND_DATA00_QSPI_A_DATA0		0x82
			MX8MM_IOMUXC_NAND_DATA01_QSPI_A_DATA1		0x82
			MX8MM_IOMUXC_NAND_DATA02_QSPI_A_DATA2		0x82
			MX8MM_IOMUXC_NAND_DATA03_QSPI_A_DATA3		0x82
		>;
	};

	pinctrl_i2c1: i2c1grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C1_SCL_I2C1_SCL			0x400001c3
			MX8MM_IOMUXC_I2C1_SDA_I2C1_SDA			0x400001c3
		>;
	};

	pinctrl_i2c1_1: i2c1_1grp {
		fsl,pins = <
#define GP_I2C1_SCL	<&gpio5 14 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C1_SCL_GPIO5_IO14		0x400001c3
#define GP_I2C1_SDA	<&gpio5 15 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C1_SDA_GPIO5_IO15		0x400001c3
		>;
	};

	pinctrl_i2c1_pf8100: i2c1-pf8100grp {
		fsl,pins = <
#define GP_I2C1_PF8100_EWARN	<&gpio3 3 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_NAND_CE2_B_GPIO3_IO3		0x149
#define GP_I2C1_PF8100_FAULT	<&gpio3 4 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_NAND_CE3_B_GPIO3_IO4		0x149
		>;
	};

	pinctrl_i2c2: i2c2grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C2_SCL_I2C2_SCL			0x400001c3
			MX8MM_IOMUXC_I2C2_SDA_I2C2_SDA			0x400001c3
		>;
	};

	pinctrl_i2c2_1: i2c2_1grp {
		fsl,pins = <
#define GP_I2C2_SCL	<&gpio5 16 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C2_SCL_GPIO5_IO16		0x400001c3
#define GP_I2C2_SDA	<&gpio5 17 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C2_SDA_GPIO5_IO17		0x400001c3
		>;
	};

	pinctrl_i2c2_gt911: i2c2-gt911grp {
		fsl,pins = <
#define GPIRQ_GT911 		<&gpio1 6 IRQ_TYPE_LEVEL_HIGH>
#define GP_GT911_IRQ 		<&gpio1 6 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO06_GPIO1_IO6		0x1d6
			/* driver writes levels, instead of active/inactive */
#define GP_GT911_RESET		<&gpio1 7 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO07_GPIO1_IO7		0x149
		>;
	};

	pinctrl_i2c2_ft5x06: i2c2-ft5x06grp {
		fsl,pins = <
#define GPIRQ_I2C2_FT5X06	<&gpio1 6 IRQ_TYPE_EDGE_FALLING>
#define GP_I2C2_FT5X06_WAKE	<&gpio1 6 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO06_GPIO1_IO6		0x149
#define GP_I2C2_FT5X06_RESET	<&gpio1 7 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO07_GPIO1_IO7		0x109
		>;
	};

	pinctrl_i2c2_sn65dsi83: i2c2-sn65dsi83grp {
		fsl,pins = <
#define GPIRQ_I2C2_SN65DSI83	<&gpio1 1 IRQ_TYPE_LEVEL_HIGH>
			MX8MM_IOMUXC_GPIO1_IO01_GPIO1_IO1		0x04
#define GP_I2C2_SN65DSI83_EN	<&gpio1 9 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO09_GPIO1_IO9		0x106
		>;
	};

	pinctrl_i2c3: i2c3grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C3_SCL_I2C3_SCL			0x400001c3
			MX8MM_IOMUXC_I2C3_SDA_I2C3_SDA			0x400001c3
		>;
	};

	pinctrl_i2c3_1: i2c3_1grp {
		fsl,pins = <
#define GP_I2C3_SCL	<&gpio5 18 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C3_SCL_GPIO5_IO18		0x400001c3
#define GP_I2C3_SDA	<&gpio5 19 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C3_SDA_GPIO5_IO19		0x400001c3
		>;
	};

	pinctrl_i2c3a_rv4162: i2c3a-rv4162grp {
		fsl,pins = <
#define GPIRQ_RV4162		<&gpio4 22 IRQ_TYPE_LEVEL_LOW>
			MX8MM_IOMUXC_SAI2_RXC_GPIO4_IO22		0x1c0
		>;
	};

	pinctrl_i2c3b_csi1: i2c3b-csi1grp {
		fsl,pins = <
#define GP_CSI1_MIPI_PWDN	<&gpio1 8 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO08_GPIO1_IO8		0x141
#define GP_CSI1_MIPI_RESET	<&gpio3 5 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_NAND_CLE_GPIO3_IO5			0x101
		>;
	};

	pinctrl_i2c4: i2c4grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C4_SCL_I2C4_SCL			0x400001c3
			MX8MM_IOMUXC_I2C4_SDA_I2C4_SDA			0x400001c3
		>;
	};

	pinctrl_i2c4_1: i2c4_1grp {
		fsl,pins = <
#define GP_I2C4_SCL	<&gpio5 20 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C4_SCL_GPIO5_IO20		0x400001c3
#define GP_I2C4_SDA	<&gpio5 21 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_I2C4_SDA_GPIO5_IO21		0x400001c3
		>;
	};

	pinctrl_mipi_com50h5n03ulc: mipi-com50h5n03ulcgrp {
		fsl,pins = <
#define GP_MIPI_RESET	<&gpio1 9 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO09_GPIO1_IO9	0x16
		>;
	};

	pinctrl_mipi_lcm_jm430: mipi-lcm-jm430grp {
		fsl,pins = <
#define GP_TC358762_EN	<&gpio1 9 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO09_GPIO1_IO9	0x16
		>;
	};

	pinctrl_mipi_ltk0680ytmdb: mipi-ltk0680ytmdbgrp {
		fsl,pins = <
#define GP_MIPI_RESET	<&gpio1 9 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO09_GPIO1_IO9	0x16
		>;
	};

	pinctrl_mipi_ltk080a60a004t: mipi-ltk080a60a004tgrp {
		fsl,pins = <
#define GP_LTK08_MIPI_EN	<&gpio1 1 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO01_GPIO1_IO1	0x116
		>;
	};

	pinctrl_pcie0: pcie0grp {
		fsl,pins = <
#define GP_PCIE0_RESET		<&gpio4 31 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_SAI3_TXFS_GPIO4_IO31	0x100
#define GP_PCIE0_DISABLE	<&gpio1 5 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO05_GPIO1_IO5	0x100
		>;
	};

	pinctrl_pwm1: pwm1grp {
		fsl,pins = <
			MX8MM_IOMUXC_SPDIF_EXT_CLK_PWM1_OUT	0x16
		>;
	};

	pinctrl_pwm2: pwm2grp {
		fsl,pins = <
			MX8MM_IOMUXC_SPDIF_RX_PWM2_OUT		0x16
		>;
	};

	pinctrl_pwm3: pwm3grp {
		fsl,pins = <
			MX8MM_IOMUXC_SPDIF_TX_PWM3_OUT		0x16
		>;
	};

	pinctrl_pwm4: pwm4grp {
		fsl,pins = <
			MX8MM_IOMUXC_SAI3_MCLK_PWM4_OUT		0x16
		>;
	};

	pinctrl_reg_usdhc2_vqmmc: reg_usdhc2_vqmmcgrp {
		fsl,pins = <
#define GP_USDHC2_VSEL	<&gpio3 2 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_NAND_CE1_B_GPIO3_IO2	0x16
		>;
	};

	pinctrl_reg_wlan_vmmc: reg-wlan-vmmcgrp {
		fsl,pins = <
#define GP_REG_WLAN_VMMC	<&gpio3 20 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_SAI5_RXC_GPIO3_IO20	0x16
		>;
	};

	pinctrl_sai1: sai1grp {
		fsl,pins = <
			/* wm8960 */
			MX8MM_IOMUXC_SAI1_MCLK_SAI1_MCLK		0xd6
			MX8MM_IOMUXC_SAI1_TXFS_SAI1_TX_SYNC		0xd6
			MX8MM_IOMUXC_SAI1_TXC_SAI1_TX_BCLK		0xd6
			MX8MM_IOMUXC_SAI1_TXD0_SAI1_TX_DATA0		0xd6
			MX8MM_IOMUXC_SAI1_RXD0_SAI1_RX_DATA0		0xd6
		>;
	};

	pinctrl_sai2: sai2grp {
		fsl,pins = <
			/* Bluetooth PCM */
			MX8MM_IOMUXC_SAI2_TXFS_SAI2_TX_SYNC		0xd6
			MX8MM_IOMUXC_SAI2_TXC_SAI2_TX_BCLK		0xd6
			MX8MM_IOMUXC_SAI2_TXD0_SAI2_TX_DATA0		0xd6
			MX8MM_IOMUXC_SAI2_RXD0_SAI2_RX_DATA0		0xd6
		>;
	};

	pinctrl_sai3: sai3grp {
		fsl,pins = <
			/* J16 */
			MX8MM_IOMUXC_SAI3_MCLK_SAI3_MCLK		0xd6	/* Pin 22 */
			MX8MM_IOMUXC_SAI3_TXC_SAI3_TX_BCLK		0xd6	/* Pin 26 */
			MX8MM_IOMUXC_SAI3_TXD_SAI3_TX_DATA0		0xd6	/* Pin 28 */
			MX8MM_IOMUXC_SAI3_RXD_SAI3_RX_DATA0		0xd6	/* Pin 30 */
		>;
	};

	pinctrl_sai5: sai5grp {
		fsl,pins = <
			MX8MM_IOMUXC_SAI5_MCLK_SAI5_MCLK		0xd6
			MX8MM_IOMUXC_SAI5_RXFS_SAI5_RX_SYNC		0xd6
			MX8MM_IOMUXC_SAI5_RXD0_SAI5_RX_DATA0		0xd6
			MX8MM_IOMUXC_SAI5_RXD1_SAI5_RX_DATA1    	0xd6
			MX8MM_IOMUXC_SAI5_RXD2_SAI5_RX_DATA2    	0xd6
			MX8MM_IOMUXC_SAI5_RXD3_SAI5_RX_DATA3    	0xd6
		>;
	};

	pinctrl_sound_wm8960: sound-wm8960grp {
		fsl,pins = <
#define GP_WM8960_MIC_DET		<&gpio1 10 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_GPIO1_IO10_GPIO1_IO10		0x80
#define GP_WM8960_HP_DET		<&gpio4 28 GPIO_ACTIVE_HIGH>
			MX8MM_IOMUXC_SAI3_RXFS_GPIO4_IO28		0x80
		>;
	};

	pinctrl_uart1: uart1grp {
		fsl,pins = <
			MX8MM_IOMUXC_UART1_RXD_UART1_DCE_RX		0x140
			MX8MM_IOMUXC_UART1_TXD_UART1_DCE_TX		0x140
			MX8MM_IOMUXC_UART3_RXD_UART1_DCE_CTS_B		0x140
			MX8MM_IOMUXC_UART3_TXD_UART1_DCE_RTS_B		0x140
		>;
	};

	pinctrl_uart2: uart2grp {
		fsl,pins = <
			MX8MM_IOMUXC_UART2_RXD_UART2_DCE_RX		0x140
			MX8MM_IOMUXC_UART2_TXD_UART2_DCE_TX		0x140
		>;
	};

	pinctrl_uart3: uart3grp {
		fsl,pins = <
			MX8MM_IOMUXC_ECSPI1_SCLK_UART3_DCE_RX		0x140
			MX8MM_IOMUXC_ECSPI1_MOSI_UART3_DCE_TX		0x140
			MX8MM_IOMUXC_ECSPI1_SS0_UART3_DCE_RTS_B		0x140
			MX8MM_IOMUXC_ECSPI1_MISO_UART3_DCE_CTS_B	0x140
		>;
	};

	pinctrl_uart4: uart4grp {
		fsl,pins = <
			MX8MM_IOMUXC_UART4_RXD_UART4_DCE_RX		0x140
			MX8MM_IOMUXC_UART4_TXD_UART4_DCE_TX		0x140
		>;
	};

	pinctrl_usbotg1: usbotg1grp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO12_USB1_OTG_PWR		0x16
			MX8MM_IOMUXC_GPIO1_IO13_USB1_OTG_OC		0x16
		>;
	};

	pinctrl_usbotg2: usbotg2grp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO14_USB2_OTG_PWR		0x16
			MX8MM_IOMUXC_GPIO1_IO15_USB2_OTG_OC		0x16
		>;
	};

	pinctrl_usdhc1: usdhc1grp {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x190
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d0
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d0
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d0
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d0
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d0
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d0
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d0
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d0
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d0
#define GP_EMMC_RESET		<&gpio2 10 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_SD1_RESET_B_GPIO2_IO10		0x141
		>;
	};

	pinctrl_usdhc1_100mhz: usdhc1grp100mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x194
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d4
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d4
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d4
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d4
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d4
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d4
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d4
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d4
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d4
		>;
	};

	pinctrl_usdhc1_200mhz: usdhc1grp200mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x196
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d6
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d6
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d6
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d6
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d6
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d6
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d6
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d6
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d6
		>;
	};

	pinctrl_usdhc2: usdhc2grp {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x190
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d0
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d0
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d0
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d0
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d0
#define GP_USDHC2_CD	<&gpio2 12 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_SD2_CD_B_GPIO2_IO12		0x1c4
		>;
	};

	pinctrl_usdhc2_100mhz: usdhc2grp100mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x194
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d4
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d4
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d4
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d4
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d4
		>;
	};

	pinctrl_usdhc2_200mhz: usdhc2grp200mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x196
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d6
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d6
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d6
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d6
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d6
		>;
	};

	pinctrl_usdhc3: usdhc3grp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x190
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d0
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d0
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d0
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d0
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d0
			/* Bluetooth slow clock */
			MX8MM_IOMUXC_GPIO1_IO00_ANAMIX_REF_CLK_32K	0x03
		>;
	};

	pinctrl_usdhc3_100mhz: usdhc3grp100mhz {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x194
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d4
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d4
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d4
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d4
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d4
		>;
	};

	pinctrl_usdhc3_200mhz: usdhc3grp200mhz {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x196
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d6
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d6
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d6
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d6
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d6
		>;
	};

	pinctrl_wdog: wdoggrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO02_WDOG1_WDOG_B		0x140
		>;
	};

	pinctrl_wdog_gpio: wdog-gpiogrp {
		fsl,pins = <
#define GP_WDOG_RESET	<&gpio1 2 GPIO_ACTIVE_LOW>
			MX8MM_IOMUXC_GPIO1_IO02_GPIO1_IO2		0x140
		>;
	};
};

/ {
	model = "Boundary Devices i.MX8MMini Nitrogen8MM";
	compatible = "boundary,imx8mm-nitrogen8mm", "fsl,imx8mm";

	chosen {
		bootargs = "console=ttymxc1,115200 earlycon=ec_imx6q,0x30890000,115200";
		stdout-patch = &uart2;
	};

#if 0
	backlight_mipi: backlight-mipi {
		brightness-levels = <0 1 2 3 4 5 6 7 8 9 10>;
		compatible = "pwm-backlight";
		default-brightness-level = <8>;
		display = <&display_subsystem>;
		pwms = <&pwm3 0 30000>;		/* 33.3 Khz */
		status = "okay";
	};

	bt-rfkill {
		compatible = "net,rfkill-gpio";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_bt_rfkill>;
		name = "bt-rfkill";
		type = <2>; /* Bluetooth */
		reset-gpios = GP_BT_RFKILL_RESET;
		status = "okay";
	};

	csi_mclk: csi-mclk {
		compatible = "pwm-clock";
		#clock-cells = <0>;
		clock-frequency = <20000000>;
		clock-output-names = "csi_mclk";
		pwms = <&pwm2 0 50>; /* 1 / 50 ns = 20 MHz */
	};
#endif

	reg_usdhc2_vqmmc: regulator-usdhc2-vqmmc {
		compatible = "regulator-gpio";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_usdhc2_vqmmc>;
		regulator-name = "reg_sd2_vsel";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <3300000>;
		regulator-type = "voltage";
		regulator-boot-on;
		regulator-always-on;
		gpios = GP_USDHC2_VSEL;
		states = <1800000 0x1
			  3300000 0x0>;
	};

	reg_vref_1v8: regulator-vref-1v8 {
		compatible = "regulator-fixed";
		regulator-name = "vref-1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	reg_vref_2v5: regulator-vref-2v5 {
		compatible = "regulator-fixed";
		regulator-name = "vref-2v5";
		regulator-min-microvolt = <2500000>;
		regulator-max-microvolt = <2500000>;
	};

	reg_vref_3v3: regulator-vref-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "vref-3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	reg_vref_5v: regulator-vref-5v {
		compatible = "regulator-fixed";
		regulator-name = "vref-5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	reg_wlan_vmmc: regulator-wlan-vmmc {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_wlan_vmmc>;
		regulator-name = "reg_wlan_vmmc";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = GP_REG_WLAN_VMMC;
		startup-delay-us = <70000>;
		enable-active-high;
	};

#if 0
	sound-wm8960 {
		compatible = "fsl,imx-audio-wm8960";
		model = "wm8960-audio";
		cpu-dai = <&sai1>;
		codec-master;
		audio-codec = <&wm8960>;
		audio-routing =
			"Headphone Jack", "HP_L",
			"Headphone Jack", "HP_R",
			"Ext Spk", "SPK_LP",
			"Ext Spk", "SPK_LN",
			"Ext Spk", "SPK_RP",
			"Ext Spk", "SPK_RN",
			"LINPUT1", "Main MIC",
			"Main MIC", "MICB",
			"RINPUT1", "Mic Jack",
			"Mic Jack", "MICB";
		/* JD2: hp detect high for headphone*/
		hp-det = <2 0>;
		hp-det-gpios = GP_WM8960_HP_DET;
#if 0		/* Jack is not stuffed */
		mic-det-gpios = GP_WM8960_MIC_DET;
#endif
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_sound_wm8960>;
	};
#endif
};

#if 0
&A53_0 {
	arm-supply = <&reg_sw4>;
};

&clk {
	assigned-clocks = <&clk IMX8MM_AUDIO_PLL1>, <&clk IMX8MM_AUDIO_PLL2>;
	assigned-clock-rates = <786432000>, <722534400>;
};

&csi1_bridge {
	fsl,mipi-mode;
	status = "okay";

	port {
		csi1_ep: endpoint {
			remote-endpoint = <&csi1_mipi_ep>;
		};
	};
};

&ecspi2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_ecspi2>;
	fsl,spi-num-chipselects = <1>;
	cs-gpios = GP_ECSPI2_CS0;
	status = "okay";
	#address-cells = <1>;
	#size-cells = <0>;

	spidev@0 {
		compatible = "spidev";
		spi-max-frequency = <2000000>;
		reg = <0>;
	};
};
#endif

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_fec1>;
#if 0
	phy-reset-gpios = GP_FEC1_RESET;
#endif
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	fsl,magic-packet;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@4 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <4>;
			interrupts-extended = GPIRQ_FEC1_PHY;
		};
	};
};

&flexspi0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_flexspi>;
	status = "okay";
};

#if 0
&gpu {
	status = "okay";
};
#endif

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c1>;
	pinctrl-1 = <&pinctrl_i2c1_1>;
	scl-gpios = GP_I2C1_SCL;
	sda-gpios = GP_I2C1_SDA;
	status = "okay";

#if 0
	pf8100@08 {
		compatible = "nxp,pf8x00";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c1_pf8100>;
		reg = <0x08>;

		regulators {
			reg_ldo1: ldo1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <5000000>;
				regulator-min-microvolt = <1500000>;
			};

			reg_ldo2: ldo2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <5000000>;
				regulator-min-microvolt = <1500000>;
#if 0
				/* vselect low is 3.3V, high is 1.8V */
				vselect-en;
#endif
			};

			reg_ldo3: ldo3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <5000000>;
				regulator-min-microvolt = <1500000>;
			};

			reg_ldo4: ldo4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <5000000>;
				regulator-min-microvolt = <1500000>;
			};

			reg_sw1: sw1 {
				phase = <0>;
				ilim-ma = <4500>;
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
			};

			reg_sw2: sw2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
			};

			reg_sw3: sw3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
			};

			reg_sw4: sw4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
				dual-phase;
			};

			reg_sw5: sw5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
			};

			reg_sw6: sw6 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <1800000>;
				regulator-min-microvolt =  <400000>;
			};

			reg_sw7: sw7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <4100000>;
				regulator-min-microvolt = <1000000>;
			};

			reg_vsnvs: vsnvs {
				regulator-always-on;
				regulator-boot-on;
				regulator-max-microvolt = <3300000>;
				regulator-min-microvolt = <1800000>;
			};
		};
	};
#endif
};

&i2c2 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c2>;
	pinctrl-1 = <&pinctrl_i2c2_1>;
	scl-gpios = GP_I2C2_SCL;
	sda-gpios = GP_I2C2_SDA;
	status = "okay";

#if 0
	mipi_to_lvds: mipi-to-lvds@2c {
		clocks = <&mipi_dsi 0>,
			<&clk IMX8MM_CLK_LCDIF_PIXEL_DIV>;
		clock-names = "mipi_clk", "pixel_clock";
		compatible = "ti,sn65dsi83";
		display = <&display_subsystem>;
		display-dsi = <&fb_mipi>;
		enable-gpios = GP_I2C2_SN65DSI83_EN;
		interrupts-extended = GPIRQ_I2C2_SN65DSI83;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c2_sn65dsi83>;
		reg = <0x2c>;
		status = "okay";
	};

	touchscreen@38 {
		compatible = "ft5x06-ts";
		interrupts-extended = GPIRQ_I2C2_FT5X06;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c2_ft5x06>;
		reg = <0x38>;
		wakeup-gpios = GP_I2C2_FT5X06_WAKE;
		reset-gpios = GP_I2C2_FT5X06_RESET;
	};

	touchscreen@5d {
		compatible = "goodix,gt9271";
		display = <&display_subsystem>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c2_gt911>;
		reg = <0x5d>;
		esd-recovery-timeout-ms = <2000>;
		interrupts-extended = GPIRQ_GT911;
		irq-gpios = GP_GT911_IRQ;
		reset-gpios = GP_GT911_RESET;
	};
#endif
};

&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c3>;
	pinctrl-1 = <&pinctrl_i2c3_1>;
	scl-gpios = GP_I2C3_SCL;
	sda-gpios = GP_I2C3_SDA;
	status = "okay";

	i2cmux@70 {
		compatible = "nxp,pca9540";
		reg = <0x70>;
		#address-cells = <1>;
		#size-cells = <0>;

		i2c3a: i2c3@0 {
			reg = <0>;
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c3b: i2c3@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;
		};
	};
};

#if 0
&i2c3a {
	rtc@68 {
		compatible = "microcrystal,rv4162";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c3a_rv4162>;
		reg = <0x68>;
		interrupts-extended = GPIRQ_RV4162;
		wakeup-source;
	};
};

&i2c3b {
	ov5640-mipi1@3c {
		compatible = "ov5640_mipisubdev";
		reg = <0x3c>;
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_i2c3b_csi1>;
		clocks = <&csi_mclk>;
		clock-names = "csi_mclk";
		csi_id = <0>;
		AVDD-supply = <&reg_vref_2v5>;
		DVDD-supply = <&reg_vref_3v3>;
		DOVDD-supply = <&reg_vref_1v8>;
		pwn-gpios = GP_CSI1_MIPI_PWDN;
		rst-gpios = GP_CSI1_MIPI_RESET;
		mclk = <20000000>;
		mipi_csi;

		port {
			ov5640_mipi1_ep: endpoint {
				remote-endpoint = <&mipi1_sensor_ep>;
			};
		};
	};
};
#endif

&i2c4 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c4>;
	pinctrl-1 = <&pinctrl_i2c4_1>;
	scl-gpios = GP_I2C4_SCL;
	sda-gpios = GP_I2C4_SDA;
	status = "okay";

	wm8960: codec@1a {
		compatible = "wlf,wm8960";
		reg = <0x1a>;
		clocks = <&clk IMX8MM_CLK_SAI1_ROOT>;
		clock-names = "mclk";
		wlf,shared-lrclk;
	};
};

#if 0
&lcdif {
	status = "okay";
};

&mipi_csi_1 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi1_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi1_ep>;
			data-lanes = <2>;
			csis-hs-settle = <13>;
			csis-clk-settle = <2>;
			csis-wclk;
		};

		csi1_mipi_ep: endpoint2 {
			remote-endpoint = <&csi1_ep>;
		};
	};
};

&mipi_dsi {
	assigned-clocks = <&clk IMX8MM_CLK_DSI_CORE_SRC>,
			  <&clk IMX8MM_CLK_DSI_PHY_REF_SRC>,
			  <&clk IMX8MM_VIDEO_PLL1_REF_SEL>,
			  <&clk IMX8MM_CLK_LCDIF_PIXEL_DIV>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_266M>,
				 <&clk IMX8MM_VIDEO_PLL1_OUT>,
				 <&clk IMX8MM_CLK_24M>;
	assigned-clock-rates = <266000000>, <0>, <0>, <66000000>;

	clocks = <&clk IMX8MM_CLK_DSI_CORE_DIV>,
		 <&clk IMX8MM_CLK_DSI_PHY_REF_DIV>,
		 <&clk IMX8MM_CLK_LCDIF_PIXEL_DIV>;
	clock-names = "cfg", "pll-ref", "pixel_clock";
	status = "okay";

	fb_mipi: panel@0 {
		bits-per-color = <8>;
		bridge-de-active = <0>;
		bus-format = "rgb888";
		compatible = "panel,simple";
		dsi-format = "rgb888";
		dsi-lanes = <4>;
#if 0
		/* u-boot will set this where appropriate */
		enable-gpios = GP_LTK08_MIPI_EN;
		mipi-cmds = <&mipi_cmds_ltk080a60a004t>;
#endif
		mode-skip-eot;
		mode-video;
#if 1
		mode-video-burst;
#else
		mode-video-sync-pulse;
#endif
		min-hs-clock-multiple = <8>;
		panel-height-mm = <136>;
		panel-width-mm = <217>;
		power-supply = <&reg_vref_5v>;
		reg = <0>;
		spwg;

		display-timings {
			t_mipi: t-dsi-default {
				/* m101nwwb by default */
				clock-frequency = <66000000>;
				hactive = <1280>;
				vactive = <800>;
				hback-porch = <5>;
				hfront-porch = <123>;
				vback-porch = <3>;
				vfront-porch = <24>;
				hsync-len = <1>;
				vsync-len = <1>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <1>;
			};
		};
	};
};

&mu {
	status = "okay";
};

&pcie0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie0>;
	disable-gpio = GP_PCIE0_DISABLE;
	reset-gpio = GP_PCIE0_RESET;
	ext_osc = <0>;
	status = "okay";
};

&pwm1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm1>;
	status = "okay";
};

&pwm2 {
	assigned-clocks = <&clk IMX8MM_CLK_PWM2_SRC>,
			<&clk IMX8MM_CLK_PWM2_DIV>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_40M>;
	assigned-clock-rates = <0>, <40000000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm2>;
	status = "okay";
};

&pwm3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm3>;
	status = "okay";
};

&pwm4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm4>;
	status = "okay";
};

&sai1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai1>;
	assigned-clocks = <&clk IMX8MM_CLK_SAI1_SRC>,
			<&clk IMX8MM_CLK_SAI1_DIV>;
	assigned-clock-parents = <&clk IMX8MM_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <12288000>;
	status = "okay";
};

&sai2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai2>;
	status = "okay";
};

&sai3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai3>;
	status = "disabled";
};

&sai5 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai5>;
	status = "disabled";
};

&uart1 { /* BT */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	assigned-clocks = <&clk IMX8MM_CLK_UART1_SRC>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_80M>;
	fsl,uart-has-rtscts;
	status = "okay";
};
#endif

&uart2 { /* console */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	assigned-clocks = <&clk IMX8MM_CLK_UART2_SRC>;
	assigned-clock-parents = <&clk IMX8MM_CLK_24M>;
	status = "okay";
};

#if 0
&uart3 { /* J15 */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	assigned-clocks = <&clk IMX8MM_CLK_UART3_SRC>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_80M>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4>;
	assigned-clocks = <&clk IMX8MM_CLK_UART4_SRC>;
	assigned-clock-parents = <&clk IMX8MM_CLK_24M>;
	status = "okay";
};
#endif

&usbotg1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usbotg1>;
	power-polarity-active-high;
	dr_mode = "otg";
	status = "okay";
};

&usbotg2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usbotg2>;
	power-polarity-active-high;
	dr_mode = "host";
	status = "okay";
};

&usdhc1 {
	cap-mmc-highspeed;
	bus-width = <8>;
	mmc-ddr-1_8v;
	mmc-hs200-1_8v;
	no-mmc-hs400;
	non-removable;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc1>;
	pinctrl-1 = <&pinctrl_usdhc1_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc1_200mhz>;
	reset-gpios = GP_EMMC_RESET;
	status = "okay";
	vmmc-supply = <&reg_vref_3v3>;
	vqmmc-1-8-v;
	vqmmc-supply = <&reg_vref_1v8>;
};

&usdhc2 {
	bus-width = <4>;
	cap-sd-highspeed;
	cd-gpios = GP_USDHC2_CD;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>;
	sd-uhs-ddr50;
	sd-uhs-sdr104;
	sd-uhs-sdr12;
	sd-uhs-sdr25;
	status = "okay";
#if 1
	vqmmc-supply = <&reg_usdhc2_vqmmc>;
#else
	vqmmc-supply = <&reg_ldo2>;
#endif
};

#if 0
&usdhc3 {
	bus-width = <4>;
	no-sd-uhs-sdr104;
	non-removable;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc3>;
	pinctrl-1 = <&pinctrl_usdhc3_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc3_200mhz>;
	status = "okay";
	vmmc-supply = <&reg_wlan_vmmc>;
	vqmmc-1-8-v;
};

&vpu_g1 {
	status = "okay";
};

&vpu_g2 {
	status = "okay";
};

&vpu_h1 {
	status = "okay";
};
#endif

&wdog1 {
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_wdog>;
	pinctrl-1 = <&pinctrl_wdog_gpio>;
	reset-gpios = GP_WDOG_RESET;
	fsl,ext-reset-output;
	status = "okay";
};
