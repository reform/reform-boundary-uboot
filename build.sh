#!/bin/bash

set -exu

if [ -z ${SOURCE_DATE_EPOCH:+x} ] && git -C . rev-parse 2>/dev/null; then
	export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)
fi
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm

# avoid rebuilding everything every time
export KBUILD_NOCMDDEP=1

make -j$(nproc) flash.bin
